import { render } from '@testing-library/react-native';
import React from 'react';
import App from '../App';
import { HomeScreen } from '../src/scripts/components/HomeScreen';
import { mockComponent } from '../src/scripts/components/__mocks__/mockComponent';

const [MockedHomeScreen] = mockComponent<typeof HomeScreen>();

describe('App', () => {
  it('should render properly', () => {
    render(<App />);
  });
});

jest.mock('../src/scripts/modules/store', () => ({
  store: {},
}));

jest.mock('../src/scripts/components/HomeScreen', () => ({
  HomeScreen: (props) => <MockedHomeScreen {...props} />,
}));
