import i18n from 'i18n-js';

export const translations = {
  en: {
    'blocked.not_available_yet': 'Next exercises will be available in',
    'blocked.please_come_back_later': 'Please come back later',
    'exercise.confirm': 'confirm',
    'exercise.i_don_t_know': "I don't know",
    'result.continue': 'continue',
  },
};

i18n.translations = translations;
i18n.locale = 'en';
i18n.defaultSeparator = ' => ';

type Scope = keyof typeof translations['en'];

export const t = (scope: Scope | Scope[], options?: i18n.TranslateOptions) => {
  return i18n.t(scope, options);
};
