import { storage } from '../storage';
import AsyncStorage from '@react-native-async-storage/async-storage';

const someString = 'some-string';

describe('storage', () => {
  describe('getItem', () => {
    it('should forward the correct key and resolve with a parse object', async () => {
      await expect(storage.getItem(someString)).resolves.toEqual({ key: someString });
      expect(AsyncStorage.getItem).toHaveBeenCalledWith(someString);
    });
  });

  describe('setItem', () => {
    it('should forward the corect key and resolve', async () => {
      await expect(storage.setItem(someString, { key: someString })).resolves.toBeUndefined();
      expect(AsyncStorage.setItem).toHaveBeenCalledWith(someString, `{"key":"${someString}"}`);
    });
  });

  describe('removeItem', () => {
    it('should forward the correct key and resolve', async () => {
      await expect(storage.removeItem(someString)).resolves.toBeUndefined();
      expect(AsyncStorage.removeItem).toHaveBeenCalledWith(someString);
    });
  });
});

jest.mock('@react-native-async-storage/async-storage', () => ({
  getItem: jest.fn().mockImplementation((key: string) => Promise.resolve('{"key": "' + key + '"}')),
  setItem: jest.fn().mockImplementation((key: string) => Promise.resolve()),
  removeItem: jest.fn().mockImplementation((key: string) => Promise.resolve()),
}));
