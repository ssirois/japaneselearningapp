import React from 'react';
import { Text } from '@ui-kitten/components';
import { useStoreState } from '../modules/store';
import { t } from '../modules/translation';
import { msToTime } from '../modules/msToTime';
import { Card } from './styled/Card';
import { CardHeader } from './styled/CardHeader';
import { CardBody } from './styled/CardBody';

interface BlockedProps {
  proceedToNextWord: () => void;
}

export const Blocked: React.FC<BlockedProps> = ({ proceedToNextWord }) => {
  const [countdown, setCountdown] = React.useState(null);
  const getNextWordDueDate = useStoreState((state) => state.getNextWordDueDate);

  React.useEffect(() => {
    const intervalId = setInterval(() => {
      const timeLeft = getNextWordDueDate() - Date.now();
      const shouldProceedToNextWord = timeLeft <= 0;
      setCountdown(timeLeft);

      if (shouldProceedToNextWord) {
        proceedToNextWord();
      }
    }, 100);

    return () => {
      clearInterval(intervalId);
    };
  });

  return (
    <Card>
      <CardHeader>
        <Text category="h6">{t('blocked.please_come_back_later')}</Text>
      </CardHeader>
      <CardBody>
        <Text>
          {t('blocked.not_available_yet')} {msToTime(countdown)}
        </Text>
      </CardBody>
    </Card>
  );
};
