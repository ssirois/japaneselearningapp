import React from 'react';
import { Button, Input, Text, useTheme } from '@ui-kitten/components';
import { toHiragana, toRomaji } from 'wanakana';
import { Platform, View } from 'react-native';
import { getWordDescription, getWordInfos, Word } from '../modules/dictionary';
import { Planet } from 'react-kawaii/lib/native/';
import { t } from '../modules/translation';
import { CardFooter } from './styled/CardFooter';
import { Character } from './styled/Character';
import { css } from '@emotion/native';
import { Card } from './styled/Card';
import { CardHeader } from './styled/CardHeader';
import { CardBody } from './styled/CardBody';

interface TextAnswerProps {
  onWrongAnswer: () => void;
  onRightAnswer: () => void;
  word: Word;
}

export const TextAnswer: React.FC<TextAnswerProps> = ({ onWrongAnswer, onRightAnswer, word }) => {
  const description = getWordDescription(word.id);
  const infos = getWordInfos(word.id);
  const [value, setValue] = React.useState('');
  const theme = useTheme();

  const handleAnswer = () => {
    if (toHiragana(value) === toHiragana(toRomaji(word.kana[0].text))) {
      onRightAnswer();
    } else {
      onWrongAnswer();
    }
  };

  return (
    <Card status="primary">
      <CardHeader>
        <Text category="h6">{description}</Text>
        {infos?.map((info, i) => (
          <Text category="s2" key={i}>
            {info}
          </Text>
        ))}
      </CardHeader>
      <CardBody>
        <Character>
          {value ? (
            <Planet size={200} mood="blissful" color={theme['color-primary-default']} />
          ) : (
            <Planet size={200} mood="happy" color={theme['color-primary-default']} />
          )}
        </Character>
        <Input
          value={toHiragana(value)}
          onKeyPress={(e) => {
            if (e.nativeEvent.key.match('Enter') && value) {
              handleAnswer();
            }
          }}
          onChangeText={(currentValue) => {
            setValue(toRomaji(currentValue));
          }}
          /*
           * Work around to fix an issue with controlled inputs
           * Should be fixed by this pull request. https://github.com/facebook/react-native/pull/29070
           */
          secureTextEntry={Platform.OS === 'android'}
          keyboardType={
            /* istanbul ignore next */
            Platform.OS === 'android' ? 'visible-password' : null
          }
          testID={'text-input'}
        />
      </CardBody>
      <CardFooter>
        <View
          style={css`
            padding-right: 10px;
          `}
        >
          <Button
            onPress={() => {
              onWrongAnswer();
            }}
            status="warning"
            appearance="outline"
            testID="i-dont-know-button"
          >
            {t('exercise.i_don_t_know')}
          </Button>
        </View>
        <View
          style={css`
            padding-left: 10px;
          `}
        >
          <Button status="primary" onPress={handleAnswer} disabled={!value} testID="confirm-button">
            {t('exercise.confirm')}
          </Button>
        </View>
      </CardFooter>
    </Card>
  );
};
