export const みんなの日本語3 = [
  {
    id: 'みんなの日本語3.1',
    jmdictId: '1288810',
    kana: [{ text: 'ここ' }],
    kanji: [{ text: '此処' }],
    sense: [
      {
        gloss: [
          { text: 'here (place physically close to the speaker, place pointed by the speaker while explaining)' },
          { text: 'this place' },
        ],
        misc: ['uk'],
        partOfSpeech: ['pn'],
      },
    ],
  },
  {
    id: 'みんなの日本語3.2',
    jmdictId: '1006670',
    kana: [{ text: 'そこ' }],
    kanji: [{ text: '其処' }],
    sense: [
      {
        gloss: [{ text: 'there (place relatively near listener)' }],
        misc: ['uk'],
        partOfSpeech: ['pn'],
      },
    ],
  },
  {
    id: 'みんなの日本語3.3',
    jmdictId: '1000320',
    kana: [{ text: 'あそこ' }],
    kanji: [{ text: '彼処' }],
    sense: [
      {
        gloss: [
          { text: 'there (place physically distant from both speaker and listener)' },
          { text: 'over there' },
          { text: 'that place' },
        ],
        misc: ['uk'],
        partOfSpeech: ['pn'],
      },
    ],
  },
  {
    id: 'みんなの日本語3.4',
    jmdictId: '1577140',
    kana: [{ text: 'どこ' }],
    kanji: [{ text: '何処' }],
    sense: [
      {
        gloss: [{ text: 'where' }, { text: 'what place' }],
        misc: ['uk'],
        partOfSpeech: ['pn'],
      },
    ],
  },
  {
    id: 'みんなの日本語3.5',
    jmdictId: '1004500',
    kana: [{ text: 'こちら' }],
    kanji: [{ text: '此方' }],
    sense: [
      {
        gloss: [
          { text: 'this way (direction close to the speaker or towards the speaker)' },
          { text: 'this direction' },
        ],
        misc: ['uk'],
        partOfSpeech: ['n'],
      },
    ],
  },
  {
    id: 'みんなの日本語3.6',
    jmdictId: '1006780',
    kana: [{ text: 'そちら' }],
    kanji: [{ text: '其方' }],
    sense: [
      {
        gloss: [{ text: 'that way (direction distant from the speaker, close to the listener)' }],
        misc: ['uk'],
        partOfSpeech: ['pn'],
      },
    ],
  },
  {
    id: 'みんなの日本語3.7',
    jmdictId: '1483185',
    kana: [{ text: 'あちら' }],
    kanji: [{ text: '彼方' }],
    sense: [
      {
        gloss: [
          { text: 'that way (direction distant from both speaker and listener)' },
          { text: 'over there' },
          { text: 'yonder' },
        ],
        misc: ['uk'],
        partOfSpeech: ['pn'],
      },
    ],
  },
  {
    id: 'みんなの日本語3.8',
    jmdictId: '1237150',
    kana: [{ text: 'きょうしつ' }],
    kanji: [{ text: '教室' }],
    sense: [{ gloss: [{ text: 'classroom' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語3.9',
    jmdictId: '1358550',
    kana: [{ text: 'しょくどう' }],
    kanji: [{ text: '食堂' }],
    sense: [
      {
        gloss: [
          { text: 'dining room' },
          { text: 'dining hall' },
          { text: 'cafeteria' },
          { text: 'canteen' },
          { text: 'messroom' },
        ],
        partOfSpeech: ['n'],
      },
    ],
  },
  {
    id: 'みんなの日本語3.10',
    jmdictId: '1314400',
    kana: [{ text: 'じむしょ' }],
    kanji: [{ text: '事務所' }],
    sense: [{ gloss: [{ text: 'office' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語3.11',
    jmdictId: '1198380',
    kana: [{ text: 'かいぎしつ' }],
    kanji: [{ text: '会議室' }],
    sense: [{ gloss: [{ text: 'conference room' }, { text: 'council room' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語3.12',
    jmdictId: '1588060',
    kana: [{ text: 'うけつけ' }],
    kanji: [{ text: '受付' }],
    sense: [
      {
        gloss: [{ text: 'reception (desk)' }, { text: 'information desk' }],
        info: ['esp. 受付'],
        partOfSpeech: ['n'],
      },
    ],
  },
  {
    id: 'みんなの日本語3.13',
    jmdictId: '1147800',
    kana: [{ common: true, text: 'ロビー' }],
    sense: [{ gloss: [{ text: 'lobby' }, { text: 'lounge' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語3.14',
    jmdictId: '1499320',
    kana: [{ text: 'へや' }],
    kanji: [{ text: '部屋' }],
    sense: [{ gloss: [{ text: 'room' }, { text: 'chamber' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語3.15',
    jmdictId: '1084810',
    kana: [{ text: 'トイレ' }],
    sense: [
      {
        gloss: [{ text: 'toilet' }, { text: 'restroom' }, { text: 'bathroom' }, { text: 'lavatory' }],
        misc: ['abbr'],
        partOfSpeech: ['n'],
      },
    ],
  },
  {
    id: 'みんなの日本語3.16',
    jmdictId: '1198760',
    kana: [{ text: 'かいだん' }],
    kanji: [{ text: '会談' }],
    sense: [
      {
        gloss: [
          { text: 'conversation' },
          { text: 'conference (usu. between important people)' },
          { text: 'discussion' },
          { text: 'interview' },
        ],
        partOfSpeech: ['n', 'vs'],
      },
    ],
  },
  {
    id: 'みんなの日本語3.17',
    jmdictId: '1030630',
    kana: [{ text: 'エレベーター' }],
    sense: [{ gloss: [{ text: 'elevator' }, { text: 'lift' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語3.18',
    jmdictId: '1028580',
    kana: [{ text: 'エスカレーター' }],
    sense: [{ gloss: [{ text: 'escalator' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語3.19',
    jmdictId: '1318480',
    kana: [{ text: 'じどうはんばいき' }],
    kanji: [{ text: '自動販売機' }],
    sense: [{ gloss: [{ text: 'vending machine' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語3.20',
    jmdictId: '1443840',
    kana: [{ text: 'でんわ' }],
    kanji: [{ text: '電話' }],
    sense: [
      {
        gloss: [{ text: 'telephone (device)' }, { text: 'phone' }],
        misc: ['abbr'],
        partOfSpeech: ['n', 'adjNo'],
      },
    ],
  },
  {
    id: 'みんなの日本語3.21',
    jmdictId: '1592250',
    kana: [{ text: 'くに' }],
    kanji: [{ text: '国' }],
    sense: [{ gloss: [{ text: 'country' }, { text: 'state' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語3.22',
    jmdictId: '1198550',
    kana: [{ text: 'かいしゃ' }],
    kanji: [{ text: '会社' }],
    sense: [{ gloss: [{ text: 'company' }, { text: 'corporation' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語3.23',
    jmdictId: '1191740',
    kana: [{ text: 'うち' }],
    kanji: [{ text: '家' }],
    sense: [{ gloss: [{ text: 'house' }], misc: ['uk'], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語3.24',
    jmdictId: '1246700',
    kana: [{ text: 'くつ' }],
    kanji: [{ text: '靴' }],
    sense: [
      {
        gloss: [{ text: 'shoe' }, { text: 'shoes' }, { text: 'boots' }, { text: 'footwear' }, { text: 'footgear' }],
        partOfSpeech: ['n'],
      },
    ],
  },
  {
    id: 'みんなの日本語3.25',
    jmdictId: '1092820',
    kana: [{ text: 'ネクタイ' }],
    sense: [{ gloss: [{ text: 'tie' }, { text: 'necktie' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語3.26',
    jmdictId: '1148850',
    kana: [{ text: 'ワイン' }],
    sense: [{ gloss: [{ text: 'wine' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語3.27',
    jmdictId: '1588550',
    kana: [{ text: 'うりば' }],
    kanji: [{ text: '売り場' }],
    sense: [
      {
        gloss: [
          { text: 'selling area' },
          { text: 'counter' },
          { text: 'section' },
          { text: 'department' },
          { text: 'sales floor' },
        ],
        partOfSpeech: ['n'],
      },
    ],
  },
  {
    id: 'みんなの日本語3.28',
    jmdictId: '1420840',
    kana: [{ text: 'ちか' }],
    kanji: [{ text: '地下' }],
    sense: [
      {
        gloss: [{ text: 'basement' }, { text: 'cellar' }, { text: 'underground place' }],
        partOfSpeech: ['n', 'adjNo'],
      },
    ],
  },
  {
    id: 'みんなの日本語3.29',
    jmdictId: '1203020',
    kana: [{ text: 'かい' }],
    kanji: [{ text: '階' }],
    sense: [{ gloss: [{ text: '-th floor' }], partOfSpeech: ['n', 'nSuf'] }],
  },
  {
    id: 'みんなの日本語3.30',
    jmdictId: '1175570',
    kana: [{ text: 'えん' }],
    kanji: [{ text: '円' }],
    sense: [{ gloss: [{ text: 'yen (Japanese monetary unit)' }], partOfSpeech: ['n'] }],
  },
  {
    id: 'みんなの日本語3.31',
    jmdictId: '1219980',
    kana: [{ text: 'いくら' }],
    kanji: [{ text: '幾ら' }],
    sense: [{ gloss: [{ text: 'how much?' }, { text: 'how many?' }], misc: ['uk'], partOfSpeech: ['adv', 'n'] }],
  },
  {
    id: 'みんなの日本語3.32',
    jmdictId: '1488000',
    kana: [{ text: 'ひゃく' }],
    kanji: [{ text: '百' }],
    sense: [
      {
        gloss: [{ text: '100' }, { text: 'hundred' }],
        info: ['陌 and 佰 are used in legal documents'],
        partOfSpeech: ['num'],
      },
    ],
  },
  {
    id: 'みんなの日本語3.33',
    jmdictId: '1388740',
    kana: [{ text: 'せん' }],
    kanji: [{ text: '千' }],
    sense: [
      {
        gloss: [{ text: '1,000' }, { text: 'thousand' }],
        info: ['阡 and 仟 are used in legal documents'],
        partOfSpeech: ['num'],
      },
    ],
  },
  {
    id: 'みんなの日本語3.34',
    jmdictId: '1584460',
    kana: [{ text: 'まん' }],
    kanji: [{ text: '万' }],
    sense: [
      {
        gloss: [{ text: '10,000' }, { text: 'ten thousand' }],
        info: ['萬 is sometimes used in legal documents'],
        partOfSpeech: ['num'],
      },
    ],
  },
  {
    id: 'みんなの日本語3.35',
    jmdictId: '1295060',
    kana: [{ text: 'すみません' }],
    kanji: [{ text: '済みません' }],
    sense: [
      {
        gloss: [{ text: 'excuse me' }, { text: 'pardon me' }, { text: "I'm sorry" }],
        info: ["used both to apologize and to get someone's attention"],
        misc: ['uk', 'pol'],
        partOfSpeech: ['exp', 'int'],
      },
    ],
  },
  {
    id: 'みんなの日本語3.36',
    jmdictId: '1009000',
    kana: [{ text: 'どうも' }],
    sense: [
      {
        gloss: [{ text: 'thank you' }, { text: 'thanks' }],
        misc: ['abbr'],
        partOfSpeech: ['int'],
        related: [['どうも有難う']],
      },
    ],
  },
  {
    id: 'みんなの日本語3.37',
    jmdictId: '1000930',
    kana: [{ text: 'いらっしゃいませ' }],
    sense: [{ gloss: [{ text: 'welcome' }], info: ['used in shops, restaurants, etc.'], partOfSpeech: ['exp'] }],
  },
  {
    id: 'みんなの日本語3.38',
    jmdictId: '1184270',
    kana: [{ text: 'ください' }],
    kanji: [{ text: '下さい' }],
    sense: [{ gloss: [{ text: 'please (give me)' }], misc: ['uk', 'hon'], partOfSpeech: ['exp'] }],
  },
  {
    id: 'みんなの日本語3.39',
    jmdictId: '1005900',
    kana: [{ text: 'じゃあ' }],
    sense: [
      {
        gloss: [{ text: 'then' }, { text: 'well' }, { text: 'so' }, { text: 'well then' }],
        info: ['from では'],
        partOfSpeech: ['conj'],
        related: [['では', 1]],
      },
    ],
  },
];
