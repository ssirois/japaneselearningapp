import styled from '@emotion/native';

export const CardFooter = styled.View`
  padding: 16px 24px;
  flex-direction: row;
  justify-content: flex-end;
  flex-shrink: 0;
`;
