import React from 'react';
import { TopNavigation, useTheme } from '@ui-kitten/components';
import { Success } from './Success';
import { Fail } from './Fail';
import { Exercise } from './Exercise';
import { useStoreActions, WordModel } from '../modules/store';
import { Blocked } from './Blocked';
import { Rescue } from './Rescue';
import { Layout } from './styled/Layout';
import { Main } from './styled/Main';
import { ExerciseStatus } from '../modules/ExerciseStatus';
import { ThemeProvider } from '@emotion/react';
import { css } from '@emotion/native';
import { findWord } from '../modules/dictionary';

export const HomeScreen = () => {
  const [currentWord, setCurrentWord] = React.useState<WordModel>();
  const [status, setStatus] = React.useState<ExerciseStatus>(ExerciseStatus.BlOCKED);
  const [lastAnswer, setLastAnswer] = React.useState('');
  const { getNextWord, incrementWordProgress, decrementWordProgress, refreshLastEncounterDate } = useStoreActions(
    (actions) => actions,
  );
  const theme = useTheme();

  React.useEffect(() => {
    getNextWord().then((word) => {
      if (word) {
        setStatus(ExerciseStatus.PENDING);
        setCurrentWord(word);
      } else {
        setStatus(ExerciseStatus.BlOCKED);
        setCurrentWord(null);
      }
    });
  }, []);

  const proceedToNextWord = () => {
    getNextWord().then((word) => {
      if (word) {
        setStatus(ExerciseStatus.PENDING);
        setCurrentWord(word);
      } else {
        setStatus(ExerciseStatus.BlOCKED);
        setCurrentWord(null);
      }
    });
  };

  return (
    <ThemeProvider theme={theme}>
      <TopNavigation alignment="center" />
      <Layout theme={theme}>
        <Main
          style={css`
            flex: 1;
            padding: 30px 0;
          `}
        >
          {(() => {
            switch (status) {
              case ExerciseStatus.SUCCESS:
                return <Success word={findWord(currentWord.id)} proceedToNextWord={proceedToNextWord} />;
              case ExerciseStatus.RESCUE:
                return <Rescue word={findWord(currentWord.id)} proceedToNextWord={proceedToNextWord} />;
              case ExerciseStatus.FAIL:
                return (
                  <Fail word={findWord(currentWord.id)} lastAnswer={lastAnswer} proceedToNextWord={proceedToNextWord} />
                );
              case ExerciseStatus.PENDING:
                return (
                  <Exercise
                    onSuccess={() => {
                      incrementWordProgress(currentWord.id);
                      setStatus(ExerciseStatus.SUCCESS);
                    }}
                    onRescue={() => {
                      refreshLastEncounterDate(currentWord.id);
                      setStatus(ExerciseStatus.RESCUE);
                    }}
                    onFail={() => {
                      decrementWordProgress(currentWord.id);
                      setStatus(ExerciseStatus.FAIL);
                    }}
                    setLastAnswer={setLastAnswer}
                    word={findWord(currentWord?.id)}
                  />
                );
              default:
                return <Blocked proceedToNextWord={proceedToNextWord} />;
            }
          })()}
        </Main>
      </Layout>
    </ThemeProvider>
  );
};
