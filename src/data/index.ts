import { Word } from '../scripts/modules/dictionary';
import { みんなの日本語1 } from './みんなの日本語1';
import { みんなの日本語2 } from './みんなの日本語2';
import { みんなの日本語3 } from './みんなの日本語3';
import { みんなの日本語4 } from './みんなの日本語4';
import { みんなの日本語5 } from './みんなの日本語5';

export const words: Word[] = [
  ...みんなの日本語1,
  ...みんなの日本語2,
  ...みんなの日本語3,
  ...みんなの日本語4,
  ...みんなの日本語5,
];
